#! /bin/bash

IMAGE=localhost:5000/core/stretch
if [ -z "${BUILD_URL}" ]; then
  # not running under Jenkins
  IMAGE_TAG=${IMAGE_TAG:-manual}
  EFS=${EFS:-${PWD}/efs}
else
  # running under Jenkins
  case "${ENVIRONMENT}" in
    itlab)
      IMAGE_TAG=${IMAGE_TAG:-nightly}
      ;;
    *)
      IMAGE_TAG=${IMAGE_TAG:-latest}
      ;;
  esac
  EFS=${EFS:-/efs}
fi

HOST_BUILD_DIR=${HOST_BUILD_DIR:-${PWD}}
DOCKER_BUILD_DIR=${DOCKER_BUILD_DIR:-/build}
DOCKER_CONF_DIR=${DOCKER_CONF_DIR:-/etc/apache2}

if ! type -f docker >/dev/null 2>&1; then
  echo Failed to find docker command >&2
  exit 1
fi

[ $# -eq 0 ] && set -- ${ENVIRONMENT}

if [ $# -lt 1 ]; then
  echo "Usage: $(basename $0) envname [envname...]" >&2
  echo "  OR" >&2
  echo "Usage: export ENVIRONMENT=envname; $(basename $0)" >&2
  exit 1
fi

for env in $*; do
  ENV=$(echo $env|sed s/-/_/g)

  # for now, we need to separate the base directory (/efs or $PWD/efs)
  # from the remaining path (data/idp_apache/${env})

  HOST_CONF_BASE=${HOST_CONF_BASE:-${EFS}}
  HOST_CONF_PATH=${HOST_CONF_PATH:-data/idp_apache/${env}}

  # remove any leading period from $SUFFIX
  SUFFIX=${SUFFIX#.}
  [ "${SUFFIX}" = "prev" ] && SUFFIX=

  # pull the image first
  pre_run="docker pull ${IMAGE}:${IMAGE_TAG}"; 
  ${pre_run}

  # we're going to use this several times, so make it easy
  run="docker run -t -v ${EFS}:/efs ${IMAGE}:${IMAGE_TAG}"

  # in production (no SUFFIX) we want to remove the old .prev
  # directory, then rename the old production directory (so existing
  # bind mounts are not broken), and create a new directory

  # in test we want to remove the old suffixed directory and create a
  # new one

  # get rid of any previous suffixed directories (defaults to .prev)
  ${run} rm -rf /efs/${HOST_CONF_PATH}.${SUFFIX:-prev}

  # rename any existing .prev directory
  [ -z "${SUFFIX}" ] && \
    ${run} test -d /efs/${HOST_CONF_PATH} && \
    ${run} mv /efs/${HOST_CONF_PATH} /efs/${HOST_CONF_PATH}.prev

  # add SUFFIX to HOST_CONF_PATH, if it's set
  HOST_CONF_PATH=${HOST_CONF_PATH}${SUFFIX:+.}${SUFFIX}

  # make a new config directory
  ${run} mkdir -p /efs/${HOST_CONF_PATH}

  HOST_CONF_DIR=${HOST_CONF_BASE}/${HOST_CONF_PATH}

  docker run -t \
    -e HOST_CONF_DIR=${HOST_CONF_DIR} \
    -e DOCKER_CONF_DIR=${DOCKER_CONF_DIR} \
    -e HOST_BUILD_DIR=${HOST_BUILD_DIR} \
    -e DOCKER_BUILD_DIR=${DOCKER_BUILD_DIR} \
    -e ENV=${ENV} \
    -v ${HOST_BUILD_DIR}:${DOCKER_BUILD_DIR}:ro \
    -v ${HOST_CONF_DIR}:${DOCKER_CONF_DIR} \
    ${IMAGE}:${IMAGE_TAG} \
    ${DOCKER_BUILD_DIR}/build.sh
done
