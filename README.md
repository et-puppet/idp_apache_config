# idp_apache_config

Builds Apache configuration for proxying to clusters of Shibboleth IdPs.

You can test it locally with `./test.sh`, assuming you have a local
`localhost:5000/core/stretch:manual` image. If you don't, you can run
the [local registry](https://code.stanford.edu/et/local-registry/)
image with your S3 credentials, and pull the nightly build and re-tag
it:

    % docker pull localhost:5000/core/stretch:nightly
    % docker tag  localhost:5000/core/stretch:{nightly,manual}

## TODO

Add multiple environments
