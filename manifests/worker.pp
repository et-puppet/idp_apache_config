# Class: idp_apache_config::worker
# ===========================
#
# Generate a configuration for the Apache worker MPM - really just a
# wrapper around apache::mod::worker
#
# (see https://github.com/puppetlabs/puppetlabs-apache/blob/master/manifests/mod/worker.pp for details and defaults)
#
# Parameters
# ----------
#
# Parameters should come from hiera; 
#
#  [*startservers*]
#   (optional) The number of child server processes created on startup
#
#  [*maxclients*]
#   (optional) The max number of simultaneous requests that will be served.
#   This is the old name and is still supported. The new name is
#   MaxRequestWorkers as of 2.3.13.
#
#  [*minsparethreads*]
#   (optional) Minimum number of idle threads to handle request spikes.
#
#  [*maxsparethreads*]
#   (optional) Maximum number of idle threads.
#
#  [*threadsperchild*]
#   (optional) The number of threads created by each child process.
#
#  [*maxrequestsperchild*]
#   (optional) Limit on the number of connectiojns an individual child server
#   process will handle. This is the old name and is still supported. The new
#   name is MaxConnectionsPerChild as of 2.3.9+.
#
#  [*serverlimit*]
#   (optional) With worker, use this directive only if your MaxRequestWorkers
#   and ThreadsPerChild settings require more than 16 server processes
#   (default). Do not set the value of this directive any higher than the
#   number of server processes required by what you may want for
#   MaxRequestWorkers and ThreadsPerChild.
#
#  [*threadlimit*]
#   (optional) This directive sets the maximum configured value for
#   ThreadsPerChild for the lifetime of the Apache httpd process.
#
#  [*listenbacklog*]
#    (optional) Maximum length of the queue of pending connections.
#    Default is '128' - Limited by the Linux kernel
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
# Vivien Wu <vivienwu@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2018 The Board of Trustees of the Leland Stanford Junior
# University
#
define idp_apache_config::worker (
  $startservers,
  $maxclients,
  $minsparethreads,
  $maxsparethreads,
  $threadsperchild,
  $maxrequestsperchild,
  $serverlimit,
  $threadlimit,
  $listenbacklog,
) {
  class { 'apache::mod::worker':
    startservers        => $startservers,
    maxclients          => $maxclients,
    minsparethreads     => $minsparethreads,
    maxsparethreads     => $maxsparethreads,
    threadsperchild     => $threadsperchild,
    maxrequestsperchild => $maxrequestsperchild,
    serverlimit         => $serverlimit,
    threadlimit         => $threadlimit,
    listenbacklog       => $listenbacklog,
  }
}
