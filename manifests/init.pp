# Class: idp_apache_config
# ===========================
#
# Generate a configuration for Apache servers in front of IdPs with
# certificate authentication
#
# Parameters
# ----------
#
# Parameters should come from hiera
#
# * `vhost_defaults`
#   default values for vhosts, overriden by explicit values in `vhosts`
#
# * `vhosts`
#   a hash of hashes, keyed by the internal name of the IdP (which is available in vhosts.pp as $name)
#
#   * `admin`
#     the admin email address (string, required)
#   * `domain`
#     the public domain for the IdP (e.g. stanford.edu) (string, required)
#   * `backend_count`
#     the number of backend servers (in vhost_defaults this should match the default desired value in deploy.sh) (string/number, required)
#   * `log_dir`
#     the Apache log directory for this vhost (string, default: '/var/log/apache2')
#   * `ensure`
#     ensure value for Apache::Vhost resource (string, default: 'present')
#   * `proxy_protocol_exceptions`
#     IP addresses or CIDR ranges that will connect without Proxy Protocol (array of strings, default: [ '127.0.0.1' ])
#   * `realname`
#     the real/new (non-FQDN) name of the IdP (e.g. login) (string, default: "${realname_prefix}${name}")
#   * `realname_prefix`
#     a prefix for realname (string, default: '')
#   * `oldname`
#     the old name of the IdP (e.g. idp) (string, default: "${oldname_prefix}${name}"
#   * `oldname_prefix`
#     a prefix for oldname (string, default: '')
#   * `backend_domain`
#     the domain (e.g. cluster.local) for the backends (string, default: $domain)
#   * `backend_port`
#     the port number for the backend servers - vhost backends must listen on the same port (string/number, default: 443)
#   * `backend_name_source`
#     which name to use for the backend - can be 'name', 'realname', or 'oldname' (string, default: 'realname')
#
# * `worker`
#   a hash of key / value pairs for configuring the worker module (hash, default: undef)
#
# Examples
# --------
#
# @example
#   include 'idp_apache_config'
#
# assumes that there is data in hiera defining $vhost_defaults and
# $vhosts:
#
#  # data/itlab.yaml
#  ---
#  idp_apache_config::vhost_defaults:
#    localdomain: cluster.local
#    backend_count: 2
#    realname: login
#    oldname: weblogin
#    log_dir: /var/log/apache2
#    domain: itlab.stanford.edu
#    admin: emerging-tech@lists.stanford.edu
#  idp_apache_config::vhosts:
#    login: {}
#    login-dev:
#      domain: dev.itlab.stanford.edu
#      ensure: absent
#
# Or:
#
#  # data/authnz_x.yaml
#  # Puppet does not allow hyphen / dash in environment names,
#  # so this needs to be authnz_x.yaml, not authnz-x.yaml
#  ---
#  idp_apache_config::vhost_defaults:
#    domain: stanford.edu
#    localdomain: cluster.local
#    admin: saml-team@lists.stanford.edu
#    # ensure this matches the default setting for desired in deploy.sh
#    backend_count: 3
#    log_dir: /var/log/apache2
#    realname_prefix: login-
#    oldname_prefix: idp-
#    backend_name_source: 'oldname'
#  idp_apache_config::vhosts:
#    dev:
#      backend_count: 2
#      backend_port: 9445
#    test:
#      backend_count: 1
#      backend_port: 9451
#    uat:
#      backend_port: 9443
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
# Vivien Wu <vivienwu@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2018 The Board of Trustees of the Leland Stanford Junior
# University
#
class idp_apache_config (
  $vhost_defaults,
  $vhosts,
  $worker = undef,
) {

  class { [
    'stdlib',
    'apt'
    ]:
  }

  Exec['apt_update'] -> Package<| |>

  $mpm_module = $worker ? {
    undef   => 'worker',
    default => 'false', # lint:ignore:quoted_booleans
  }

  class { 'apache':
    service_enable      => false,
    service_ensure      => false,
    service_manage      => false,
    servername          => "idpproxy.${vhost_defaults['domain']}",
    serveradmin         => $vhost_defaults['admin'],
    mpm_module          => $mpm_module,
    default_mods        => false,
    default_confd_files => false,
    default_vhost       => false,
    log_formats         => {
      vhost_common => '%v %h %l %u %t \"%r\" %>s %b',
      combined_elb => '%v %{c}a %a %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"'
    },
  }

  if $worker != undef {
    create_resources(idp_apache_config::worker, { 'worker' => $worker})
    #  class { 'idp_apache_config::worker': }
  }

  apache::mod {
    [
      'env',
      'proxy_balancer',
      'proxy_hcheck',
      'remoteip',
      'status',
      'lbmethod_byrequests',
      'slotmem_shm',
      'authn_core',
      'authn_file',
      'auth_basic',
      'authz_user',
    ]:
  }

  create_resources(idp_apache_config::vhost, $vhosts, $vhost_defaults)

  file { '/etc/apache2/conf.d/05-rotatelogs.conf':
    ensure => file,
    owner  => '0',
    group  => '0',
    mode   => '0644',
    source => "puppet:///modules/${module_name}/05-rotatelogs.conf",
  }

}
