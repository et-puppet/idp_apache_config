#
# Create an apache::vhost for the IdP apache proxy
#
# see init.pp for documentation
#
define idp_apache_config::vhost (
  $admin,
  $domain,
  $backend_count,
  $backend_hcinterval,
  $backend_hcfails,
  $backend_hcpasses,
  $realname_prefix = '',
  $oldname_prefix = '',
  $realname = "${realname_prefix}${name}",
  $oldname = "${oldname_prefix}${name}",
  $backend_domain = $domain,
  $backend_port = 443,
  $log_dir = '/var/log/apache2',
  $proxy_protocol_exceptions = [ '127.0.0.1' ],
  $ensure = 'present',
  $backend_name_source = 'realname',
) {

  # choose a name for the backend
  $backend_name = $backend_name_source ? {
    'name'    => $name,
    'oldname' => $oldname,
    default   => $realname
  }

  $proxyproto_fragments = [
    'RemoteIPProxyProtocol On',
    "RemoteIPProxyProtocolExceptions ${proxy_protocol_exceptions.join(' ')}",
  ]

  $server_status_fragments = [
   '<Location "/server-status">',
   'SetHandler server-status',
   'Require ip 127.0.0.1',
   '</Location>',
  ]

  $the_custom_fragments = concat ($proxyproto_fragments,$server_status_fragments)

  # customize the vhost configuration
  # (first parameter to concat must be an array)
  $vhost_fragments = concat(
    ["ProxyHCExpr md_valid {hc('body') =~ /urn:oasis:names:tc:SAML:2.0:nameid-format:persistent/}"],
    [ "<Proxy balancer://${backend_name}>" ],
    range('1', $backend_count).map | Integer $backend_id | {
      "  BalancerMember https://${backend_name}-${backend_id}.${backend_domain}:${backend_port} route=${backend_name}-${backend_id} enablereuse=on retry=5 ttl=120 hcmethod=GET hcexpr=md_valid hcuri=/idp/shibboleth hcinterval=${backend_hcinterval} hcfails=${backend_hcfails} hcpasses=${backend_hcpasses}"
    },
    '</Proxy>',
    $proxyproto_fragments,
    'RequestHeader set X-Client-Port "%{REMOTE_PORT}e"',
    'PassEnv SERVER_INSTANCE',
  )

  # customize the configuration for /idp/Authn/X509
  # require a client cert, and forward it to the backend
  $x509_fragment = '
    SSLVerifyClient optional
    SSLVerifyDepth 5
    SSLRequireSSL
    SSLOptions +ExportCertData +StdEnvVars +OptRenegotiate

    # add whatever SSL_* variables needed to pass to web application
    RequestHeader set SSL_CLIENT_CERT "%{SSL_CLIENT_CERT}s"
    RequestHeader set SSL_CIPHER "%{SSL_CIPHER}s"
    RequestHeader set SSL_SESSION_ID "%{SSL_SESSION_ID}s"
    RequestHeader set SSL_CIPHER_USEKEYSIZE "%{SSL_CIPHER_USEKEYSIZE}s"'


  apache::vhost { $name:
    ensure                      => $ensure,
    port                        => 443,
    ssl                         => true,
    ssl_honorcipherorder        => 'on',
    ssl_protocol                => [ '-all', '+TLSv1.1', '+TLSv1.2'],
    ssl_cert                    => "/etc/ssl/certs/${name}.pem",
    ssl_chain                   => '/etc/ssl/certs/ca-chain.pem',
    ssl_key                     => "/etc/ssl/private/${name}.key",
    ssl_ca                      => '/etc/ssl/certs/ca-client.pem',
    ssl_verify_client           => 'none',
    ssl_proxyengine             => true,
    #
    # BEFORE we fix tomcat certs
    #
    ssl_proxy_verify            => 'none',
    ssl_proxy_check_peer_cn     => 'off',
    ssl_proxy_check_peer_name   => 'off',
    ssl_proxy_check_peer_expire => 'off',
    #
    # AFTER we fix tomat certs:
    #
    # ssl_proxy_ca_cert           => '/etc/ssl/certs/ca-cluster.pem',
    # ssl_proxy_verify            => 'require',
    # ssl_proxy_verify_depth      => 1,
    # ssl_proxy_check_peer_cn     => 'on',
    # ssl_proxy_check_peer_name   => 'on',
    # ssl_proxy_check_peer_expire => 'on',
    ssl_cipher                  => 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA', # lint:ignore:140chars
    docroot                     => '/var/www',
    docroot_owner               => 'root',
    docroot_group               => 'www-data',
    docroot_mode                => '0755',
    servername                  => "${realname}.${domain}",
    serveraliases               => [ "${oldname}.${domain}" ],
    serveradmin                 => $admin,
    access_log_file             => "${log_dir}/${name}_access.log",
    error_log_file              => "${log_dir}/${name}_error.log",
    proxy_preserve_host         => true,
    custom_fragment             => $vhost_fragments.join("\n  "),
    headers                     => [
      'add Set-Cookie "instance=%{SERVER_INSTANCE}e;path=/" env=SERVER_INSTANCE',
    ],
    directories                 => [
      {
        path            => '/var/www',
      },
      {
        path            => '/idp/Authn/X509',
        provider        => 'location',
        custom_fragment => $x509_fragment,
      },
    ],
    proxy_pass                  => [
      {
        path   => '/',
        url    => "balancer://${backend_name}/",
        params => {
          lbmethod      => 'byrequests',
          stickysession => 'JSESSIONID'
        }
      }
    ],
    rewrites                    => [
      {
        comment      => 'add REMOTE_PORT to the environment for passing to Tomcat',
        rewrite_rule => ['.* - [E=REMOTE_PORT:%{REMOTE_PORT},NE]'],
      },
    ],
  }

  apache::vhost { "${name}-redirect":
    ensure          => $ensure,
    port            => 80,
    ssl             => false,
    docroot         => '/var/www',
    servername      => "${realname}.${domain}",
    serveraliases   => [ "${oldname}.${domain}" ],
    serveradmin     => $admin,
    access_log_file => "${log_dir}/${name}-redirect_access.log",
    error_log_file  => "${log_dir}/${name}-redirect_error.log",
    custom_fragment => $the_custom_fragments.join("\n  "),
    rewrites        => [
      {
        comment      => 'redirect /idp to HTTPS',
        rewrite_rule => ['/(idp/.*) https://%{SERVER_NAME}/$1 [R,L]'],
      },
      {
        comment      => 'reject other URLs',
        rewrite_rule => ['!server-status - [F]'],
      },
    ],
  }

}
