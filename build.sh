#! /bin/bash

prog=$(basename $0)

PATH=$PATH:/opt/puppetlabs/bin:/opt/puppetlabs/puppet/bin

echo ENV=${ENV}
PUPPET_ENV_DIR=/etc/puppetlabs/code/environments/${ENV}

rm -rf ${PUPPET_ENV_DIR}
mkdir -p ${PUPPET_ENV_DIR}

cd ${PUPPET_ENV_DIR}

cat >>${PUPPET_ENV_DIR}/Puppetfile <<PUPPETFILE
forge "https://forgeapi.puppetlabs.com"
mod "suet-idp_apache_config",
  :path => "${DOCKER_BUILD_DIR}"
PUPPETFILE

librarian-puppet install --clean --verbose

echo about to run puppet apply --environment=${ENV} -e "'include idp_apache_config'"

puppet apply --environment=${ENV} \
             --verbose \
             -e 'include idp_apache_config'

exit $?
